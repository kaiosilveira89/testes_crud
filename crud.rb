agenda =  {
    Kaio: '98919-7602',
    Vania: '98833-2779',
    Edilvan: '98801-2945'
}

while true
  puts ''
  puts '1 - Adicionar Novo Contato'
  puts '2 - Atualizar Contato'
  puts '3 - Exibir Lista de Contato'
  puts '4 - Excluir Contato'
  puts ''
  print 'Escolha uma opção: '
  escolha = gets.chomp  # <--- metodo de entrada onse se captura e converte o dado para string
  puts ''

  puts escolha

  case escolha
  when '1' # <--- aqui está como string pois a variavel escolha é uma sting
    print 'Insira o nome do novo contato: '
    nome = gets.chomp
    if agenda[nome.to_sym].nil?          #  .nil? Verificação se o nome já existe parametro dentro da hash agenda[]
      print 'Insira o telefone do novo contato: '
      telefone = gets.chomp              # rebendo a variavel STRING telefone
      agenda[nome.to_sym] = telefone     # a variavel telefone está sendo indexada a Key nome do contato
      puts "#{nome} foi adicionado(a) aos contatos com sucesso!"
    else
      puts "Já existe um(a) #{nome} entre seus contatos!" # #{nome} retorna o nome da Key

    end


  when '2'

    print 'Digite o nome do contato que deseja ATUALIZAR: '
    nome = gets.chomp
    if agenda[nome.to_sym].nil?
      print 'Contato inexistente!'
    else
      print 'Insira o novo numero de telefone: '
      telefone = gets.chomp
      agenda[nome.to_sym] = telefone
      puts "Novo telefone foi salvo com sucesso!"
    end

  when '3'

    agenda.each do |nome, telefone|  # O each executa para cada elemento a logica passada no bloco.
    puts "Nome: #{nome} - Telefone: #{telefone}"
    end

  when "4"

    print 'Digite o nome do contato que deseja EXCLUIR: '
    nome = gets.chomp
    if agenda[nome.to_sym].nil?
      print 'Contato inexistente!'
    else
      agenda.delete(nome.to_sym)
      puts "#{nome} foi deletado com sucesso!"
    end
  else
    puts "Opção invalida!"
    puts "Por favor, escolha novamente uma das opções apresentadas."
  end
end
